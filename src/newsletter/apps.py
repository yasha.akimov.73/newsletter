from django.apps import AppConfig

from django.db.migrations.executor import MigrationExecutor
from django.db import connections, DEFAULT_DB_ALIAS


def is_database_synchronized(database):
    connection = connections[database]
    connection.prepare_database()
    executor = MigrationExecutor(connection)
    targets = executor.loader.graph.leaf_nodes()
    return not executor.migration_plan(targets)

class NewsletterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'newsletter'

    def ready(self):
        from newsletter import signals
        from newsletter.models import NewsletterTask
        from newsletter.tasks import update_task
        
        if is_database_synchronized(DEFAULT_DB_ALIAS):
            tasks = NewsletterTask.objects.exclude(status=NewsletterTask.Status.complete)
            for task in tasks:
                update_task(task)
