from datetime import timedelta
import traceback
import random
import requests
from time import sleep

from core.celery import app
from newsletter.models import Newsletter, Client, TimeZone, Message, NewsletterTask
from django.utils import timezone


class NewsletterTaskContext:
    def __init__(self,
        newsletter: Newsletter,
        timezone: TimeZone,
        model_task: NewsletterTask=None,
        celery_task: int=None,
        delayed_time: timedelta=None
    ):
        self.newsletter = newsletter
        self.timezone = timezone
        self.model_task = model_task
        self.celery_task = celery_task
        self.delayed_time = delayed_time

    def serialise(self):
        return NewsletterTaskContext(
            self.newsletter.id,
            self.timezone.id if self.timezone else None,
            self.model_task.id if self.model_task else None,
            self.celery_task,
            self.delayed_time
        ).__dict__

    @classmethod
    def deserialise(cls, dict):
        print(f"TRY LOAD TASK {dict['model_task']} FROM DATABASE")
        return NewsletterTaskContext(
            Newsletter.objects.get(id=dict["newsletter"]),
            TimeZone.objects.get(id=dict["timezone"])  if dict["timezone"] else None,
            NewsletterTask.objects.get(id=dict["model_task"]) if dict["model_task"] else None,
            dict["celery_task"],
            dict["delayed_time"]
        )


def create_sending_tasks(newsletter: Newsletter):
    print("CREATING TASKS")
    timezones = list(TimeZone.objects.all())
    timezones.append(None)

    existing_tasks = {task.timezone: task for task in NewsletterTask.objects.filter(newsletter=newsletter)}
    print("exiting tasks", existing_tasks)
    for client_timezone in timezones:
        task_context = NewsletterTaskContext(newsletter, client_timezone)
        task_context.delayed_time = calculate_delay_time(task_context)

        if client_timezone in existing_tasks:
            task_context.model_task = existing_tasks[client_timezone]
            task_context.celery_task = existing_tasks[client_timezone].celery_task_id
            update_task(task_context)
        else:
            create_task(task_context)

def cancel_sending_tasks(newsletter: Newsletter):
    print("CANSELLING TASKS")

    for task in NewsletterTask.objects.filter(newsletter=newsletter):
        print(f"CANSELLING TASK {task}")
        app.control.revoke(task.celery_task_id)
        task.delete()

def calculate_delay_time(task_context: NewsletterTaskContext):
    print(f"calculating delay with start time {task_context.newsletter.sending_start_time}, time now {timezone.now()}, timezone {task_context.timezone.timezone if task_context.timezone else timedelta(seconds=0)}")
    delay_seconds = (
        task_context.newsletter.sending_start_time\
        - timezone.now()\
        + (task_context.timezone.timezone if task_context.timezone else timedelta(seconds=0))
    ).total_seconds()
    return max(delay_seconds, 0)

def create_task(task_context:NewsletterTaskContext):
    print("CREATING TASK")
    task_context.model_task = NewsletterTask.objects.create(
        newsletter=task_context.newsletter,
        status=NewsletterTask.Status.delayed,
        celery_task_id=None,
        timezone=task_context.timezone
    )
    task_context.model_task.save()
    print(f"SAWED TASK {task_context.model_task.id} TO DATABASE")
    submit_create_task(task_context)

def update_task(task_context:NewsletterTaskContext):
    print("UPDATING TASK")
    if task_context.model_task.status == NewsletterTask.Status.complete:
        pass
    else:
        app.control.revoke(task_context.celery_task)
        task_context.delayed_time = calculate_delay_time(task_context)
        if task_context.model_task.status == NewsletterTask.Status.delayed:
            submit_create_task(task_context)
        else:
            submit_retry_task(task_context)

def submit_create_task(task_context):
    task_context.celery_task = start_sending.apply_async(
        kwargs={"task_context": task_context.serialise()},
        countdown=task_context.delayed_time
    ).id
    task_context.model_task.celery_task_id = task_context.celery_task
    task_context.model_task.save()

def submit_retry_task(task_context):
    task_context.celery_task = retry_sending.apply_async(
        kwargs={"task_context": task_context.serialise()},
            countdown=task_context.delayed_time
    ).id
    task_context.model_task.celery_task_id = task_context.celery_task
    task_context.model_task.save()

@app.task(bind=False)
def start_sending(task_context: NewsletterTaskContext):
    print("start sending")
    print(f"task context {task_context}")
    for i in range(10):
        try:
            task_context = NewsletterTaskContext.deserialise(task_context)
        except:
            pass

    task_context.model_task.status = NewsletterTask.Status.running
    task_context.model_task.save()

    sending_end_time = task_context.newsletter.sending_end_time
    if task_context.timezone:
        sending_end_time += task_context.timezone.timezone

    client_querry = Client.objects.filter(timezone=task_context.timezone).filter(**task_context.newsletter.filter_params)
    if len(task_context.newsletter.filter_params) > 0:
        client_querry = Client.objects.filter(**task_context.newsletter.filter_params)

    all_message_done = True
    for client in client_querry.iterator():
        print("client", client)
        if timezone.now() > sending_end_time:
            task_context.model_task.status = NewsletterTask.Status.incomplete
            task_context.model_task.save()
            raise TimeoutError("Newsletter timeout")

        message = Message.objects.create(client=client, newsletter=task_context.newsletter, sending_time=timezone.now(), task=task_context.model_task)
        if not send_message(message):
            all_message_done = False

    if not all_message_done:
        print("INCOMPLETE NEWSLETTER")
        task_context.delayed_time = 0
        submit_retry_task(task_context)
    else:
        task_context.model_task.status = NewsletterTask.Status.complete
        task_context.model_task.save()


@app.task(bind=False)
def retry_sending(task_context: NewsletterTaskContext):
    print("retry sending")
    task_context = NewsletterTaskContext.deserialise(task_context)

    message_querry = Message.objects.filter(task=task_context.model_task, status=Message.Status.error)
    
    sending_end_time = task_context.newsletter.sending_end_time
    if task_context.timezone:
        sending_end_time += task_context.timezone.timezone
    
    all_message_done = True
    for message in message_querry.iterator():
        if timezone.now() > sending_end_time:
            task_context.model_task.status = NewsletterTask.Status.incomplete
            task_context.model_task.save()
            raise TimeoutError("Newsletter timeout")

        if not send_message(message):
            all_message_done = False

    if not all_message_done:
        print("INCOMPLETE NEWSLETTER")
        task_context.delayed_time = 0
        submit_retry_task(task_context)
    else:
        print("complete newsletter")
        task_context.model_task.status = NewsletterTask.Status.complete
        task_context.model_task.save()

def send_message(message: Message):
    try:
        data = {
            "id": message.id,
            "phone": message.client.phone_number,
            "text": message.newsletter.message_text
        }
        print(f"sendimg message '{data}' to {message.client}")
        responce = requests.post(
            url=f"https://probe.fbrq.cloud/v1/send/{message.id}", 
            json=data,
            headers={
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzg3OTkzMTMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Illha292QW5kcmVldmljaCJ9.hHhD5SXflt71QoBh-1sYRKYyKEADBV563yM9oGjXws0"
            }
        )
        if responce.status_code == 200:
            print(f"set message {message.id} status success")
            message.status = Message.Status.success
            message.save()
            print(f"message {message.id} saved")
            return True
        else:
            print(f"message failed {responce.status_code}, {responce.content}")
            return False
    except Exception:
        print(f"message failed exception\n{traceback.format_exc()}")
        return False
