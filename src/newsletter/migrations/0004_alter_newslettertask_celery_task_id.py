# Generated by Django 4.0.3 on 2022-03-15 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0003_remove_newsletter_task_id_alter_message_status_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newslettertask',
            name='celery_task_id',
            field=models.IntegerField(null=True),
        ),
    ]
