from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from newsletter.models import Newsletter
from newsletter.tasks import cancel_sending_tasks, create_sending_tasks

@receiver(post_save, sender=Newsletter)
def on_newsletter_update(sender, instance: Newsletter, **kwargs):
    print("UPDATED MODEL SIGNAL")
    if instance.status == Newsletter.Status.active:
        create_sending_tasks(instance)
    if instance.status == Newsletter.Status.inactive:
        cancel_sending_tasks(instance)

@receiver(pre_delete, sender=Newsletter)
def on_newsletter_update(sender, instance: Newsletter, **kwargs):
    cancel_sending_tasks(instance)
