from django.urls import path, include
from rest_framework.routers import DefaultRouter
from newsletter import views

router = DefaultRouter()
router.register(r'newsletter', views.NewsletterViewSet)
router.register(r'message', views.MessageViewSet)
router.register(r'client', views.ClientViewSet)
router.register(r'statistics', views.StatisticsView, basename="statistics")

urlpatterns = [
    path('', include(router.urls)),
]
