from newsletter.models import Newsletter, Client, Message
from rest_framework import serializers


class NewsletterSerialiser(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = '__all__'

class ClientSerialiser(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

class TimeZoneSerialiser(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

class MessageSerialiser(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['sending_time', 'newsletter', 'client']
