from django.db import models

# Create your models here.

class TimeZone(models.Model):
    timezone = models.DurationField()

class Newsletter(models.Model):
    sending_start_time = models.DateTimeField()
    sending_end_time = models.DateTimeField()
    
    message_text = models.CharField(max_length=4096)

    filter_params = models.JSONField(blank=True, default=dict)

    class Status(models.IntegerChoices):
        inactive = 1
        active = 2
    status = models.IntegerField(choices=Status.choices, default=1)

class NewsletterTask(models.Model):
    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)
    celery_task_id = models.CharField(null=True, max_length=36)

    timezone = models.ForeignKey(TimeZone, null=True, on_delete=models.SET_NULL)
    class Status(models.IntegerChoices):
        delayed = 1
        running = 2
        complete = 3
        incomplete = 4
    status = models.IntegerField(choices=Status.choices, default=1)

class Client(models.Model):
    phone_number = models.CharField(max_length=15)
    mobile_code = models.CharField(max_length=15)
    tag = models.CharField(max_length=100)
    timezone = models.ForeignKey(TimeZone, null=True, on_delete=models.SET_NULL)

class Message(models.Model):
    sending_time = models.DateTimeField()
    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)
    task = models.ForeignKey(NewsletterTask, on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    class Status(models.IntegerChoices):
        success = 1
        error = 2
    status = models.IntegerField(choices=Status.choices, default=Status.error)
