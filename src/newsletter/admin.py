from django.contrib import admin
from newsletter.models import Newsletter, Client, Message, NewsletterTask, TimeZone

class NewsletterAdmin(admin.ModelAdmin):
    pass
class ClientAdmin(admin.ModelAdmin):
    pass
class MessageAdmin(admin.ModelAdmin):
    pass
class TimeZoneAdmin(admin.ModelAdmin):
    pass
class TaskAdmin(admin.ModelAdmin):
    pass

admin.site.register(Newsletter, NewsletterAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(TimeZone, TimeZoneAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(NewsletterTask, TaskAdmin)
