from django.forms import model_to_dict
from rest_framework import viewsets, mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Count, Q
from newsletter.models import Newsletter, Client, Message, NewsletterTask
from newsletter.serialisers import (
    NewsletterSerialiser, ClientSerialiser, TimeZoneSerialiser, MessageSerialiser
)


class NewsletterViewSet(viewsets.ModelViewSet):
    serializer_class = NewsletterSerialiser
    queryset = Newsletter.objects.all()

class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerialiser
    queryset = Client.objects.all()

class TimeZoneViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = TimeZoneSerialiser
    queryset = Client.objects.all()

class MessageViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = MessageSerialiser
    queryset = Message.objects.all()


class StatisticsView(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    def get_queryset(self):
        return Newsletter.objects.all()

    def list(self, request):
        data = Newsletter.objects.annotate(
            complete=Count("newslettertask", filter=Q(newslettertask__status=NewsletterTask.Status.complete))
        ).annotate(
            delayed=Count("newslettertask", filter=Q(newslettertask__status=NewsletterTask.Status.delayed))
        ).annotate(
            incomplete=Count("newslettertask", filter=Q(newslettertask__status=NewsletterTask.Status.incomplete))
        ).annotate(
            running=Count("newslettertask", filter=Q(newslettertask__status=NewsletterTask.Status.running))
        ).values()
        
        return Response(data)

    def retrieve(self, request, pk):
        newsletter_id = pk
        data = NewsletterTask.objects.filter(newsletter_id=newsletter_id).annotate(
            success_messages_count=Count("message", filter=Q(message__status=Message.Status.success))
        ).annotate(
            error_messages_count=Count("message", filter=Q(message__status=Message.Status.error))
        ).values()

        statistics = model_to_dict(Newsletter.objects.get(id=newsletter_id))

        for task in data:
            if 'statistics' not in statistics:
                statistics['statistics'] = []
            statistics['statistics'].append(task)
        return Response(statistics)
